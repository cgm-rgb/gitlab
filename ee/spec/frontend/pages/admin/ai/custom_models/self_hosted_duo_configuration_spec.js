import { nextTick } from 'vue';
import { GlExperimentBadge } from '@gitlab/ui';
import FeatureSettingsTable from 'ee/pages/admin/ai/feature_settings/components/feature_settings_table.vue';
import SelfHostedModelsTable from 'ee/pages/admin/ai/self_hosted_models/components/self_hosted_models_table.vue';
import SelfHostedDuoConfiguration from 'ee/pages/admin/ai/custom_models/self_hosted_duo_configuration.vue';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';

describe('SelfHostedDuoConfiguration', () => {
  let wrapper;

  const createComponent = () => {
    const newSelfHostedModelPath = '/admin/ai/self_hosted_models/new';

    wrapper = shallowMountExtended(SelfHostedDuoConfiguration, {
      provide: {
        newSelfHostedModelPath,
      },
    });
  };

  beforeEach(() => {
    createComponent();
  });

  const findFeatureSettingsTab = () => wrapper.findByTestId('ai-feature-settings-tab');
  const findSelfHostedModelsTab = () => wrapper.findByTestId('self-hosted-models-tab');
  const findFeatureSettingsTable = () => wrapper.findComponent(FeatureSettingsTable);
  const findSelfHostedModelsTable = () => wrapper.findComponent(SelfHostedModelsTable);
  const findBetaBadge = () => wrapper.findComponent(GlExperimentBadge);

  it('has a title', () => {
    const title = wrapper.findByTestId('self-hosted-title');

    expect(title.text()).toBe('Self-hosted models');
  });

  it('has a beta badge', () => {
    expect(findBetaBadge().exists()).toBe(true);
  });

  it('has a description', () => {
    expect(wrapper.text()).toMatch(
      'Manage GitLab Duo by configuring and assigning self-hosted models to AI-powered features.',
    );
  });

  it('has the correct tabs', () => {
    expect(findFeatureSettingsTab().text()).toBe('AI-powered features');
    expect(findSelfHostedModelsTab().text()).toBe('Self-hosted models');
  });

  describe('self-hosted models tab', () => {
    it('renders the self-hosted models table when tab clicked', async () => {
      findSelfHostedModelsTab().vm.$emit('click');

      await nextTick();

      expect(findSelfHostedModelsTable().exists()).toBe(true);
    });
  });

  describe('feature settings tab', () => {
    it('renders the feature settings table when tab clicked', async () => {
      findFeatureSettingsTab().vm.$emit('click');

      await nextTick();

      expect(findFeatureSettingsTable().exists()).toBe(true);
    });
  });
});
