# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Ci::CreatePipelineService, feature_category: :security_policy_management do
  include RepoHelpers

  subject(:execute) { service.execute(:push, **opts) }

  let(:opts) { {} }
  let_it_be(:group) { create(:group) }
  let_it_be(:project) { create(:project, :repository, group: group) }
  let_it_be_with_reload(:compliance_project) { create(:project, :empty_repo, group: group) }
  let_it_be(:user) { create(:user, developer_of: [project, compliance_project]) }

  let(:namespace_policy_content) { { namespace_policy_job: { stage: 'build', script: 'namespace script' } } }
  let(:namespace_policy_file) { 'namespace-policy.yml' }
  let(:namespace_policy) do
    build(:pipeline_execution_policy,
      content: { include: [{
        project: compliance_project.full_path,
        file: namespace_policy_file,
        ref: compliance_project.default_branch_or_main
      }] })
  end

  let(:namespace_policy_yaml) do
    build(:orchestration_policy_yaml, pipeline_execution_policy: [namespace_policy])
  end

  let_it_be_with_reload(:namespace_policies_project) { create(:project, :empty_repo, group: group) }

  let_it_be(:namespace_configuration) do
    create(:security_orchestration_policy_configuration,
      project: nil, namespace: group, security_policy_management_project: namespace_policies_project)
  end

  let(:project_policy_content) { { project_policy_job: { script: 'project script' } } }
  let(:project_policy_file) { 'project-policy.yml' }
  let(:project_policy) do
    build(:pipeline_execution_policy,
      content: { include: [{
        project: compliance_project.full_path,
        file: project_policy_file,
        ref: compliance_project.default_branch_or_main
      }] })
  end

  let(:project_policy_yaml) do
    build(:orchestration_policy_yaml, pipeline_execution_policy: [project_policy])
  end

  let_it_be_with_reload(:project_policies_project) { create(:project, :empty_repo, group: group) }

  let_it_be(:project_configuration) do
    create(:security_orchestration_policy_configuration,
      project: project, security_policy_management_project: project_policies_project)
  end

  let(:project_ci_yaml) do
    <<~YAML
      build:
        stage: build
        script:
          - echo 'build'
      rspec:
        stage: test
        script:
          -echo 'test'
    YAML
  end

  let(:service) { described_class.new(project, user, { ref: 'master' }) }

  around do |example|
    create_and_delete_files(project, { '.gitlab-ci.yml' => project_ci_yaml }) do
      create_and_delete_files(
        project_policies_project, { '.gitlab/security-policies/policy.yml' => project_policy_yaml }
      ) do
        create_and_delete_files(
          namespace_policies_project, { '.gitlab/security-policies/policy.yml' => namespace_policy_yaml }
        ) do
          create_and_delete_files(
            compliance_project, {
              project_policy_file => project_policy_content.to_yaml,
              namespace_policy_file => namespace_policy_content.to_yaml
            }
          ) do
            example.run
          end
        end
      end
    end
  end

  before do
    stub_licensed_features(security_orchestration_policies: true)
  end

  it 'responds with success' do
    expect(execute).to be_success
  end

  it 'persists pipeline' do
    expect(execute.payload).to be_persisted
  end

  it 'persists jobs in the correct stages', :aggregate_failures do
    expect { execute }.to change { Ci::Build.count }.from(0).to(4)

    stages = execute.payload.stages
    expect(stages.map(&:name)).to contain_exactly('build', 'test')

    expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build', 'namespace_policy_job')
    expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('rspec', 'project_policy_job')
  end

  it 'sets the same partition_id for all jobs' do
    # Stub value for current partition to return one value for the first call (project pipeline)
    # and a different value for subsequent calls (policy pipelines)
    allow(::Ci::Pipeline).to receive(:current_partition_value)
                               .and_return(ci_testing_partition_id)

    builds = execute.payload.builds
    expect(builds.map(&:partition_id)).to all(eq(ci_testing_partition_id))
  end

  context 'when policy pipeline stage is not defined in the main pipeline' do
    let(:project_ci_yaml) do
      <<~YAML
        stages:
          - build
        build:
          stage: build
          script:
            - echo 'build'
      YAML
    end

    it 'responds with success' do
      expect(execute).to be_success
    end

    it 'persists the pipeline' do
      expect(execute.payload).to be_persisted
    end

    it 'ignores the policy stage', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(2)

      stages = execute.payload.stages
      expect(stages.map(&:name)).to contain_exactly('build')
      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build', 'namespace_policy_job')
    end
  end

  context 'when policy pipelines use declared, but unused project stages' do
    let(:project_ci_yaml) do
      <<~YAML
        stages:
        - build
        - test
        rspec:
          stage: test
          script:
            - echo 'rspec'
      YAML
    end

    it 'responds with success' do
      expect(execute).to be_success
    end

    it 'persists pipeline' do
      expect(execute.payload).to be_persisted
    end

    it 'persists jobs in the correct stages', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(3)

      stages = execute.payload.stages
      expect(stages.map(&:name)).to contain_exactly('build', 'test')

      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('namespace_policy_job')
      expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('rspec', 'project_policy_job')
    end
  end

  context 'when policy and project job names are not unique' do
    let(:namespace_policy_content) { { policy_job: { stage: 'build', script: 'namespace script' } } }
    let(:project_ci_yaml) do
      <<~YAML
        policy_job:
          stage: test
          script:
            - echo 'duplicate'
      YAML
    end

    shared_examples_for 'suffixed job added into pipeline' do
      it 'keeps both project job and the policy job, adding suffix for the conflicting job name', :aggregate_failures do
        expect(execute).to be_success
        expect(execute.payload).to be_persisted

        stages = execute.payload.stages
        expect(stages.find_by(name: 'build').builds.map(&:name))
          .to contain_exactly("policy_job:policy-#{namespace_policies_project.id}-0")
        expect(stages.find_by(name: 'test').builds.map(&:name))
          .to contain_exactly('policy_job', 'project_policy_job')
      end
    end

    context 'when policy uses "suffix: on_conflict"' do
      it_behaves_like 'suffixed job added into pipeline'

      context 'when policy has suffix option set explicitly to `on_conflict`' do
        let(:namespace_policy) do
          build(:pipeline_execution_policy, :suffix_on_conflict,
            content: { include: [{
              project: compliance_project.full_path,
              file: namespace_policy_file,
              ref: compliance_project.default_branch_or_main
            }] })
        end

        it_behaves_like 'suffixed job added into pipeline'
      end

      context 'when multiple policies in one policy project use the same job name' do
        let(:other_namespace_policy) do
          build(:pipeline_execution_policy,
            content: { include: [{
              project: compliance_project.full_path,
              file: namespace_policy_file,
              ref: compliance_project.default_branch_or_main
            }] })
        end

        let(:namespace_policy_yaml) do
          build(:orchestration_policy_yaml, pipeline_execution_policy: [namespace_policy, other_namespace_policy])
        end

        it 'keeps all jobs, adding suffix for the conflicting job name', :aggregate_failures do
          expect(execute).to be_success
          expect(execute.payload).to be_persisted

          stages = execute.payload.stages
          expect(stages.find_by(name: 'build').builds.map(&:name))
            .to contain_exactly(
              "policy_job:policy-#{namespace_policies_project.id}-0",
              "policy_job:policy-#{namespace_policies_project.id}-1"
            )
          expect(stages.find_by(name: 'test').builds.map(&:name))
            .to contain_exactly('policy_job', 'project_policy_job')
        end
      end

      context 'when policy job uses "needs"' do
        let(:namespace_policy_content) do
          {
            policy_job: { stage: 'build', script: 'namespace script' },
            namespace_policy_job_with_needs: {
              stage: 'test', script: 'namespace script', needs: ['policy_job']
            }
          }
        end

        let(:project_policy_content) do
          {
            policy_job: { stage: 'build', script: 'project script' },
            project_policy_job_with_needs: { script: 'project script', needs: ['policy_job'] }
          }
        end

        it 'updates needs with suffixes per pipeline for the conflicting jobs', :aggregate_failures do
          expect(execute).to be_success
          expect(execute.payload).to be_persisted

          stages = execute.payload.stages
          test_stage = stages.find_by(name: 'test')
          namespace_policy_job_with_needs = test_stage.builds.find_by(name: 'namespace_policy_job_with_needs')
          expect(namespace_policy_job_with_needs.needs.map(&:name))
            .to contain_exactly("policy_job:policy-#{namespace_policies_project.id}-0")

          project_policy_job_with_needs = test_stage.builds.find_by(name: 'project_policy_job_with_needs')
          expect(project_policy_job_with_needs.needs.map(&:name))
            .to contain_exactly("policy_job:policy-#{project_policies_project.id}-0")

          project_job = test_stage.builds.find_by(name: 'policy_job')
          expect(project_job.needs).to be_empty
        end
      end
    end

    context 'when policy uses "suffix: never"' do
      let(:namespace_policy) do
        build(:pipeline_execution_policy, :suffix_never,
          content: { include: [{
            project: compliance_project.full_path,
            file: namespace_policy_file,
            ref: compliance_project.default_branch_or_main
          }] })
      end

      it 'responds with error', :aggregate_failures do
        expect(execute).to be_error
        expect(execute.payload.errors.full_messages)
          .to contain_exactly(
            "Pipeline execution policy error: job names must be unique (policy_job)"
          )
      end
    end
  end

  context 'when any policy contains `override_project_ci` strategy' do
    let(:project_policy) do
      build(:pipeline_execution_policy, :override_project_ci,
        content: { include: [{
          project: compliance_project.full_path,
          file: project_policy_file,
          ref: compliance_project.default_branch_or_main
        }] })
    end

    it 'ignores jobs from project CI', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(2)

      stages = execute.payload.stages

      build_stage = stages.find_by(name: 'build')
      expect(build_stage.builds.map(&:name)).to contain_exactly('namespace_policy_job')
      test_stage = stages.find_by(name: 'test')
      expect(test_stage.builds.map(&:name)).to contain_exactly('project_policy_job')
    end

    context 'and policy uses custom stages' do
      let(:project_policy_content) do
        { stages: %w[build test policy-test deploy],
          project_policy_job: { stage: 'policy-test', script: 'project script' } }
      end

      it 'includes jobs with custom stages' do
        expect { execute }.to change { Ci::Build.count }.from(0).to(2)

        stages = execute.payload.stages

        build_stage = stages.find_by(name: 'build')
        expect(build_stage.builds.map(&:name)).to contain_exactly('namespace_policy_job')
        policy_test_stage = stages.find_by(name: 'policy-test')
        expect(policy_test_stage.builds.map(&:name)).to contain_exactly('project_policy_job')
      end

      context 'and also namespace policy uses `override_project_ci` with incompatible stages' do
        let(:namespace_policy) do
          build(:pipeline_execution_policy, :override_project_ci,
            content: { include: [{
              project: compliance_project.full_path,
              file: namespace_policy_file,
              ref: compliance_project.default_branch_or_main
            }] })
        end

        let(:namespace_policy_content) do
          { stages: %w[build deploy test],
            namespace_policy_job: { stage: 'test', script: 'namespace script' } }
        end

        it 'responds with error', :aggregate_failures do
          expect(execute).to be_error
          expect(execute.payload).to be_persisted
          expect(execute.payload.errors.full_messages)
            .to contain_exactly(
              'Pipeline execution policy error: Stages across `override_project_ci` policies are not compatible'
            )
        end
      end
    end

    context 'and the project has an invalid .gitlab-ci.yml' do
      let(:project_ci_yaml) do
        <<~YAML
          I'm invalid
        YAML
      end

      it 'creates the pipeline successfully' do
        expect { execute }.to change { Ci::Build.count }.from(0).to(2)
      end
    end
  end

  describe 'reserved stages' do
    context 'when policy pipelines use reserved stages' do
      let(:namespace_policy_content) do
        { namespace_pre_job: { stage: '.pipeline-policy-pre', script: 'pre script' } }
      end

      let(:project_policy_content) do
        { project_post_job: { stage: '.pipeline-policy-post', script: 'post script' } }
      end

      it 'responds with success' do
        expect(execute).to be_success
      end

      it 'persists pipeline' do
        expect(execute.payload).to be_persisted
      end

      it 'persists jobs in the reserved stages', :aggregate_failures do
        expect { execute }.to change { Ci::Build.count }.from(0).to(4)

        stages = execute.payload.stages
        expect(stages.map(&:name)).to contain_exactly('.pipeline-policy-pre', 'build', 'test', '.pipeline-policy-post')

        expect(stages.find_by(name: '.pipeline-policy-pre').builds.map(&:name)).to contain_exactly('namespace_pre_job')
        expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build')
        expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('rspec')
        expect(stages.find_by(name: '.pipeline-policy-post').builds.map(&:name)).to contain_exactly('project_post_job')
      end
    end

    context 'when reserved stages are declared in project CI YAML' do
      let(:project_ci_yaml) do
        <<~YAML
          pre-compliance:
            stage: .pipeline-policy-pre
            script:
              - echo 'pre'
          rspec:
            stage: test
            script:
              - echo 'rspec'
          post-compliance:
            stage: .pipeline-policy-post
            script:
              - echo 'post'
        YAML
      end

      it 'responds with error', :aggregate_failures do
        expect(execute).to be_error
        expect(execute.payload).to be_persisted
        expect(execute.payload.errors.full_messages)
          .to contain_exactly(
            'pre-compliance job: chosen stage `.pipeline-policy-pre` is reserved for Pipeline Execution Policies'
          )
      end
    end
  end

  context 'when policy content does not match the valid schema' do
    # A valid `content` should reference an external file via `include` and not include the jobs in the policy directly
    # The schema is defined in `ee/app/validators/json_schemas/security_orchestration_policy.json`.
    let(:namespace_policy) { build(:pipeline_execution_policy, content: namespace_policy_content) }
    let(:project_policy) { build(:pipeline_execution_policy, content: project_policy_content) }

    it 'responds with success' do
      expect(execute).to be_success
    end

    it 'persists pipeline' do
      expect(execute.payload).to be_persisted
    end

    it 'only includes project jobs and ignores the invalid policy jobs', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(2)

      stages = execute.payload.stages
      expect(stages.map(&:name)).to contain_exactly('build', 'test')

      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build')
      expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('rspec')
    end
  end

  describe 'variables precedence' do
    let(:opts) { { variables_attributes: [{ key: 'TEST_TOKEN', value: 'run token' }] } }
    let(:project_ci_yaml) do
      <<~YAML
        variables:
          TEST_TOKEN: 'global token'
        project-build:
          stage: build
          variables:
            TEST_TOKEN: 'job token'
          script:
            - echo 'build'
        project-test:
          stage: test
          script:
            - echo 'test'
      YAML
    end

    let(:project_policy_content) do
      {
        project_policy_job: {
          variables: { 'TEST_TOKEN' => 'project policy token' },
          script: 'project script'
        }
      }
    end

    let(:namespace_policy_content) do
      {
        namespace_policy_job: {
          variables: { 'TEST_TOKEN' => 'namespace policy token', 'POLICY_TOKEN' => 'namespace policy token' },
          script: 'namespace script'
        }
      }
    end

    it 'applies the policy variables in policy jobs with highest precedence', :aggregate_failures do
      stages = execute.payload.stages

      build_stage = stages.find_by(name: 'build')
      test_stage = stages.find_by(name: 'test')

      project_policy_job = test_stage.builds.find_by(name: 'project_policy_job')
      expect(get_job_variable(project_policy_job, 'TEST_TOKEN')).to eq('project policy token')

      namespace_policy_job = test_stage.builds.find_by(name: 'namespace_policy_job')
      expect(get_job_variable(namespace_policy_job, 'TEST_TOKEN')).to eq('namespace policy token')

      project_build_job = build_stage.builds.find_by(name: 'project-build')
      expect(get_job_variable(project_build_job, 'TEST_TOKEN')).to eq('run token')

      project_test_job = test_stage.builds.find_by(name: 'project-test')
      expect(get_job_variable(project_test_job, 'TEST_TOKEN')).to eq('run token')
    end

    it 'does not leak policy variables into the project jobs and other policy jobs', :aggregate_failures do
      stages = execute.payload.stages

      build_stage = stages.find_by(name: 'build')
      test_stage = stages.find_by(name: 'test')

      project_policy_job = test_stage.builds.find_by(name: 'project_policy_job')
      expect(get_job_variable(project_policy_job, 'POLICY_TOKEN')).to be_nil

      namespace_policy_job = test_stage.builds.find_by(name: 'namespace_policy_job')
      expect(get_job_variable(namespace_policy_job, 'POLICY_TOKEN')).to eq('namespace policy token')

      project_build_job = build_stage.builds.find_by(name: 'project-build')
      expect(get_job_variable(project_build_job, 'POLICY_TOKEN')).to be_nil

      project_test_job = test_stage.builds.find_by(name: 'project-test')
      expect(get_job_variable(project_test_job, 'POLICY_TOKEN')).to be_nil
    end

    context 'when project variables could disable scanners from the included security templates' do
      let(:project_policy_content) do
        {
          include: {
            template: 'Jobs/Secret-Detection.gitlab-ci.yml'
          },
          variables: {
            'SECRET_DETECTION_DISABLED' => 'false'
          }
        }
      end

      before do
        create(:ci_variable, project: project, key: 'SECRET_DETECTION_DISABLED', value: 'true')
      end

      it 'enforces policy variables to prevent scanners from being disabled' do
        stages = execute.payload.stages

        test_stage = stages.find_by(name: 'test')

        expect(test_stage.builds.map(&:name)).to include('secret_detection')
      end
    end
  end

  context 'when both Scan Execution Policy and Pipeline Execution Policy are applied on the project' do
    let(:scan_execution_policy) do
      build(:scan_execution_policy, actions: [{ scan: 'secret_detection' }])
    end

    let(:project_policy_yaml) do
      build(:orchestration_policy_yaml,
        pipeline_execution_policy: [project_policy],
        scan_execution_policy: [scan_execution_policy])
    end

    it 'persists both pipeline execution policy and scan execution policy jobs', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(5)

      expect(execute).to be_success
      expect(execute.payload).to be_persisted

      stages = execute.payload.stages
      expect(stages.map(&:name)).to contain_exactly('build', 'test')

      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build', 'namespace_policy_job')
      expect(stages.find_by(name: 'test').builds.map(&:name))
        .to contain_exactly('rspec', 'project_policy_job', 'secret-detection-0')
    end
  end

  context 'when project CI configuration is missing' do
    let(:project_ci_yaml) { nil }

    it 'responds with success' do
      expect(execute).to be_success
    end

    it 'persists pipeline' do
      expect(execute.payload).to be_persisted
    end

    it 'sets the correct config_source' do
      expect(execute.payload.config_source).to eq('pipeline_execution_policy_forced')
    end

    it 'injects the policy jobs', :aggregate_failures do
      expect { execute }.to change { Ci::Build.count }.from(0).to(2)

      stages = execute.payload.stages
      expect(stages.map(&:name)).to contain_exactly('build', 'test')

      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('namespace_policy_job')
      expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('project_policy_job')
    end
  end

  context 'when commit contains a [ci skip] directive' do
    before do
      allow_next_instance_of(Ci::Pipeline) do |instance|
        allow(instance).to receive(:git_commit_message).and_return('some message[ci skip]')
      end
    end

    it 'does not skip pipeline creation and injects policy jobs' do
      expect { execute }.to change { Ci::Build.count }.from(0).to(4)

      stages = execute.payload.stages
      expect(stages.find_by(name: 'build').builds.map(&:name)).to contain_exactly('build', 'namespace_policy_job')
      expect(stages.find_by(name: 'test').builds.map(&:name)).to contain_exactly('rspec', 'project_policy_job')
    end
  end

  describe 'access to policy configs inside security policy project repository' do
    let(:namespace_policy) do
      build(:pipeline_execution_policy,
        content: { include: [{
          project: namespace_policies_project.full_path,
          file: namespace_policy_file,
          ref: namespace_policies_project.default_branch_or_main
        }] })
    end

    let(:project_policy) do
      build(:pipeline_execution_policy,
        content: { include: [{
          project: project_policies_project.full_path,
          file: project_policy_file,
          ref: project_policies_project.default_branch_or_main
        }] })
    end

    around do |example|
      create_and_delete_files(
        project_policies_project, { project_policy_file => project_policy_content.to_yaml }
      ) do
        create_and_delete_files(
          namespace_policies_project, { namespace_policy_file => namespace_policy_content.to_yaml }
        ) do
          example.run
        end
      end
    end

    context 'when user does not have access to the policy repository' do
      it 'responds with error' do
        expect(execute).to be_error
        expect(execute.payload.errors.full_messages)
          .to contain_exactly(
            "Pipeline execution policy error: Project `#{project_policies_project.full_path}` not found " \
              "or access denied! Make sure any includes in the pipeline configuration are correctly defined.")
      end

      context 'when security policy projects have the project setting `spp_repository_pipeline_access` enabled' do
        before do
          project_policies_project.project_setting.update!(spp_repository_pipeline_access: true)
          namespace_policies_project.project_setting.update!(spp_repository_pipeline_access: true)
        end

        it 'responds with success' do
          expect(execute).to be_success
        end
      end

      context 'when group has setting `spp_repository_pipeline_access` enabled' do
        before do
          group.namespace_settings.update!(spp_repository_pipeline_access: true)
        end

        it 'responds with success' do
          expect(execute).to be_success
        end
      end

      context 'when application setting `spp_repository_pipeline_access` is enabled' do
        before do
          stub_application_setting(spp_repository_pipeline_access: true)
        end

        it 'responds with success' do
          expect(execute).to be_success
        end
      end
    end
  end

  private

  def get_job_variable(job, key)
    job.scoped_variables.to_hash[key]
  end
end
